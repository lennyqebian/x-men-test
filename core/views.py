from typing import cast

from injector import inject
from overrides import overrides
from rest_framework import status, serializers
from rest_framework.generics import CreateAPIView, GenericAPIView, RetrieveAPIView
from rest_framework.request import Request
from rest_framework.response import Response

from core.models import DnaCheck
from core.serializers import DnaCheckSerializer, DnaCheckStatsSerializer
from core.services.mutants_service import MutantsService


class SerializerAPIViewMixin(GenericAPIView):
    @overrides
    def get_serializer(self, *args, **kwargs):
        serializer = cast(self.serializer_class,
                          super().get_serializer(*args, **kwargs))
        serializer.is_valid(raise_exception=True)
        return serializer


class MutantApiView(SerializerAPIViewMixin, CreateAPIView):
    serializer_class = DnaCheckSerializer

    @inject
    def __init__(self,
                 mutants_service: MutantsService,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mutants_service = mutants_service

    @overrides
    def post(self, request: Request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            is_mutant = self.mutants_service.is_mutant(serializer.validated_data['dna'])
            request.data['is_mutant'] = is_mutant
            serializer = self.get_serializer(data=request.data)

            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            res_status = status.HTTP_200_OK if is_mutant else status.HTTP_403_FORBIDDEN

            return Response(serializer.data, status=res_status, headers=headers)
        except ValueError as err:
            raise serializers.ValidationError(err)


class StatsApiView(SerializerAPIViewMixin, RetrieveAPIView):
    serializer_class = DnaCheckStatsSerializer

    @overrides
    def get(self, request: Request, *args, **kwargs):
        count_mutant_dna: int = DnaCheck.objects.filter(is_mutant=True).count()
        count_human_dna: int = DnaCheck.objects.filter(is_mutant=False).count()
        res_data = {
            'count_mutant_dna': count_mutant_dna,
            'count_human_dna': count_human_dna,
            'ratio': count_mutant_dna / count_human_dna if count_human_dna > 0 else None
        }
        serializer = self.get_serializer(data=res_data)

        return Response(serializer.data, status=status.HTTP_200_OK)
