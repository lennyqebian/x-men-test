from django.urls import re_path

from . import views


app_name = 'persona_app'

urlpatterns = [
  re_path(
    r'mutant/?',
    views.MutantApiView.as_view(),
    name='mutant'
  ),
  re_path(
    r'stats/?',
    views.StatsApiView.as_view(),
    name='stats'
  ),
]
