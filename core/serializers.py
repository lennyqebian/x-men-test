from rest_framework import serializers

from core.models import DnaCheck


default_exclude = {
    'created',
    'modified',
}


class DnaCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = DnaCheck
        exclude = list(default_exclude.union([]))


# noinspection PyAbstractClass
class DnaCheckStatsSerializer(serializers.Serializer):
    count_mutant_dna = serializers.IntegerField()
    count_human_dna = serializers.IntegerField()
    ratio = serializers.FloatField(allow_null=True)
