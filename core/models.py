from django.db import models
from model_utils.models import TimeStampedModel


class DnaCheck(TimeStampedModel):
    dna = models.JSONField(unique=True)
    is_mutant = models.BooleanField(default=False)
