from pprint import pprint
from typing import List, Generator, Union, Any, Final

from injector import singleton

from core.types import NitrogenousBaseType, NITRO_BASES


@singleton
class MutantsService:
    MIN_DNA_SEQ_BASES: Final = 4

    def is_mutant(self, dna: List[str]) -> bool:
        self.validate_dna(dna)
        dna_columns = self.get_transpose(dna)
        dna_arrs = (dna,
                    dna_columns,
                    self.get_diagonals(dna),
                    self.get_antidiagonals(dna))

        def repeat_nitro_base(nitro_base: NitrogenousBaseType, num_times=self.MIN_DNA_SEQ_BASES) -> str:
            return nitro_base * num_times
        mutant_sequences = tuple(map(repeat_nitro_base, NITRO_BASES))

        found_mutant_sequences = sum(
            sum(dna_sequence.count(mutant_sequence)
                for dna_sequence in dna_arr
                for mutant_sequence in mutant_sequences)
            for dna_arr in dna_arrs)

        return found_mutant_sequences > 1

    def validate_dna(self, dna: List[str]):
        if not (dna is not None and len(dna) > 0):
            raise ValueError(f'¡DNA array / list cannot be empty!')
        if not (len(dna) >= self.MIN_DNA_SEQ_BASES and all(len(dna) == len(dna_sequence) for dna_sequence in dna)):
            raise ValueError(f'¡DNA array / list must be NxN (square) with N >= {self.MIN_DNA_SEQ_BASES}!')
        if not (all(
                nitro_base in NITRO_BASES
                for dna_row in dna
                for nitro_base in dna_row)):
            raise ValueError(f'¡A DNA nitrogenous base can only be one of {NITRO_BASES}!')

    @staticmethod
    def get_diagonals(matr: List[str], anti=False) -> Generator[str, Any, None]:
        width, height = len(matr[0]), len(matr)

        def get_diagonals(sub_x: int, sub_y: int
                          ) -> Generator[Union[str, List[NitrogenousBaseType]],
                                         Any, None]:
            coordinates_pairs = zip(range(sub_x, height),
                                    range(sub_y, width) if not anti
                                    else range(width - 1, sub_y - 1, -1))

            for x, y in coordinates_pairs:
                yield matr[x][y]

        for s_x in range(height):
            yield ''.join(list(get_diagonals(s_x, 0)))
        for s_y in range(1, width):
            yield ''.join(list(get_diagonals(0, s_y)))

    @classmethod
    def get_antidiagonals(cls, matr: List[str]) -> Generator[str, Any, None]:
        return cls.get_diagonals(matr, anti=True)

    @staticmethod
    def get_transpose(matr: List[str]) -> Generator[str, Any, None]:
        for x in zip(*matr):
            yield ''.join(x)


if __name__ == '__main__':
    dna_sample = ['ATGCGA',
                  'CAGTGC',
                  'TTATGT',
                  'AGAAGG',
                  'CCCCTA',
                  'TCACTG']
    mutants_service = MutantsService()

    dna_sample_diagonals = list(mutants_service.get_diagonals(dna_sample))
    print(f'dna_sample_diagonals: {len(dna_sample_diagonals)}')
    pprint(list(dna_sample_diagonals), width=50)

    dna_sample_antidiagonals = list(mutants_service.get_diagonals(dna_sample, anti=True))
    print()
    print(f'dna_sample_antidiagonals: {len(dna_sample_antidiagonals)}')
    pprint(dna_sample_antidiagonals, width=50)

    dna_sample_columns = list(mutants_service.get_transpose(dna_sample))
    print()
    print(f'dna_sample_columns: {len(dna_sample_columns)}')
    pprint(dna_sample_columns, width=50)

    print()
    print(f'DNA belongs to a mutant: {mutants_service.is_mutant(dna_sample)}')
