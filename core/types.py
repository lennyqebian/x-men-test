from typing import Literal, get_args, Final, Tuple, cast


NitrogenousBaseType = Literal['A', 'T', 'C', 'G']
NITRO_BASES: Final = cast(Tuple[NitrogenousBaseType], get_args(NitrogenousBaseType))
