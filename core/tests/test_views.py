import json
import random

from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status

from ..models import DnaCheck
from ..serializers import DnaCheckSerializer, DnaCheckStatsSerializer
from ..types import NITRO_BASES

client = Client()


class MutantApiViewTest(TestCase):
    """ Test module for MutantApiView API """

    def setUp(self):
        requests = 1000
        n = 7
        self.dna_samples = [
            {'dna': [
                ''.join(random.choices(NITRO_BASES, k=n))
                for _ in range(n)
            ]} for _ in range(requests)
        ]

    def test_mutant_endpoint(self):
        for dna_sample in self.dna_samples:
            response = client.post(reverse('persona_app:mutant'),
                                   data=dna_sample,
                                   content_type='application/json')
            dna_check = DnaCheck.objects.get(dna=dna_sample['dna'])
            serializer = DnaCheckSerializer(dna_check)

            self.assertIn(response.status_code,
                          [status.HTTP_200_OK, status.HTTP_403_FORBIDDEN])
            self.assertEqual(response.data, serializer.data)


class StatsApiViewTest(TestCase):
    def test_stats_endpoint(self):
        response = client.get(reverse('persona_app:stats'))
        count_mutant_dna: int = DnaCheck.objects.filter(is_mutant=True).count()
        count_human_dna: int = DnaCheck.objects.filter(is_mutant=False).count()
        res_data = {
            'count_mutant_dna': count_mutant_dna,
            'count_human_dna': count_human_dna,
            'ratio': count_mutant_dna / count_human_dna if count_human_dna > 0 else None
        }
        serializer = DnaCheckStatsSerializer(data=res_data)
        serializer.is_valid(raise_exception=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
