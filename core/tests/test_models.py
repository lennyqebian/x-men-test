import json

from django.test import TestCase
from ..models import DnaCheck
import random

from ..types import NITRO_BASES


class DnaCheckTest(TestCase):
    """ Test module for DnaCheck model """

    def setUp(self):
        n = 7
        self.dna_samples = [
            [
                ''.join(random.choices(NITRO_BASES, k=n))
                for _ in range(n)
            ] for _ in range(n)
        ]
        for dna_sample in self.dna_samples:
            DnaCheck.objects.create(dna=json.dumps(dna_sample), is_mutant=False)

    def test_dna_check(self):
        for dna_sample in self.dna_samples:
            dna_check = DnaCheck.objects.get(dna=json.dumps(dna_sample))

            self.assertEqual(dna_check.dna, json.dumps(dna_sample))
