# X-Men Back-end Test

## Instalación y configuración

### Requisitos del sistema

- Python v3.8 o superior
- Para la instalación de la dependencias `psycopg` y `django-heroku` se requiere que la biblioteca `libpq-dev` se encuentre disponible en el sistema operativo anfitrión.

### 1. Poetry

Asegúrese de instalar la herramienta **Poetry** para gestión de paquetes, entornos virtuales, generación y publicación de paquetes PyPI, según las indicaciones de la documentación oficial disponible en https://python-poetry.org/docs/#installation.

### 2. Dependencias definidas de este proyecto

Ejecute el siguiente comando para instalar todas las dependencias (incluidas las de desarrollo) definidas en el archivo `pyproject.toml`:

```bash
poetry install
```

Para mayor información visite https://python-poetry.org/docs/basic-usage/#installing-dependencies.

Si el entorno virtual no se inicia automáticamente, ejecute el siguiente comando para generar un nuevo intérprete de órdenes:

```bash
poetry shell
```

(Véase https://python-poetry.org/docs/basic-usage/#activating-the-virtual-environment)

### 3. Migraciones de base de datos

Para crear la base de datos y aplicar las migraciones de este proyecto, ejecute el siguiente comando desde el entorno virtual activado en el paso anterior:

```bash
python manage.py migrate
```

## Ejecución

### Servidor local

Para iniciar el servidor local, ejecute el siguiente comando desde el entorno virtual activo del proyecto:

```bash
python manage.py runserver
```

## Servicio `MutantsService` como programa

Es posible ejecutar el módulo ubicado en `core/services/mutants_service.py` como un script de Python que procesará una muestra de ADN de ejemplo.

```bash
python core/services/mutants_service.py
```

Asimismo, otra opción consiste en importar la clase `core.services.mutants_service.MutantsService`, crear una instancia de la misma y llamar al método `MutantsService.is_mutant(dna)`, el cual implementa la lógica según las condiciones del **Nivel 1**.

```python
from core.services.mutants_service import MutantsService

dna_sample = ['ATGCGA',
              'CAGTGC',
              'TTATGT',
              'AGAAGG',
              'CCCCTA',
              'TCACTG']

mutants_service = MutantsService()

mutants_service.is_mutant(dna_sample)  # boolean: True ó False
```

## Hospedaje de la solución en la Nube

Para hospedar la aplicación en la Nube, se seleccionó el servicio [**Heroku**](https://www.heroku.com/).  Se siguió los pasos descritos en un artículo oficial disponible en https://devcenter.heroku.com/articles/getting-started-with-python?singlepage=true.

## API REST

### POST `{{url_api}}/mutant/`

#### Body

- **`dna`**: Muestra de ADN a comprobar si es mutante.

#### Ejemplo de Petición

```http
POST /mutant HTTP/1.1
Content-Type: application/json
User-Agent: PostmanRuntime/7.28.0
Accept: */*
Postman-Token: 3c11de54-1102-46a7-9238-719322d0f226
Host: 127.0.0.1:8000
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Content-Length: 101
```
```json
{
    "dna": [
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCTA",
        "TCACTG"
    ]
}
```

#### Ejemplo de Respuesta Exitosa

```http
HTTP/1.1 200 OK
Date: Wed, 19 May 2021 14:01:18 GMT
Server: WSGIServer/0.2 CPython/3.8.8
Content-Type: application/json
Vary: Accept, Cookie
Allow: POST, OPTIONS
X-Frame-Options: DENY
Content-Length: 87
X-Content-Type-Options: nosniff
Referrer-Policy: same-origin
```
```json
{
  "id": 1,
  "dna": [
    "ATGCGA",
    "CAGTGC",
    "TTATGT",
    "AGAAGG",
    "CCCCTA",
    "TCACTG"
  ],
  "is_mutant": true
}
```

### GET `{{url_api}}/stats/`

#### Ejemplo de Petición

```http
GET /stats HTTP/1.1
User-Agent: PostmanRuntime/7.28.0
Accept: */*
Postman-Token: 66613e35-994e-4378-aaac-a3993fd09da0
Host: 127.0.0.1:8000
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

#### Ejemplo de Respuesta Exitosa

```http
HTTP/1.1 200 OK
Date: Wed, 19 May 2021 14:05:29 GMT
Server: WSGIServer/0.2 CPython/3.8.8
Content-Type: application/json
Vary: Accept, Cookie
Allow: GET, HEAD, OPTIONS
X-Frame-Options: DENY
Content-Length: 54
X-Content-Type-Options: nosniff
Referrer-Policy: same-origin
```
```json
{
  "count_mutant_dna": 1,
  "count_human_dna": 1,
  "ratio": 1.0
}
```
